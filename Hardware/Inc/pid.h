#ifndef _pid_h
#define _pid_h

#include "stm32f1xx_hal.h"	

typedef struct PID_Position
{
	float p;
	float i;
	float d;
	int err;
	int last_err;
	float out;
}PID1;

typedef struct PID_Add
{
  float pGain;      
  float iGain;       
  float dGain;       
  float err;       
  float last_err;  //Error[-1]   
	float prev_err;//Error[-2]
	float pidout;
}PID2;

void Position_pid_update(PID1* pid);
void Add_pid_update(PID2* pid);
#endif 
