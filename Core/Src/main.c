/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "oled.h"
#include "stdio.h"
#include "pid.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern unsigned char BMP1[];
float voltage_ISN=0.0;
float voltage_ISN_sum=0;
float voltage_ADC1=0.0;
float electric_current=0.0;
float electric_current_sum=0;
float power=0.0;
float power_want=10;//10W
float electric_current_want=1.5;//1.5A
uint32_t ADC_Value[4];
unsigned char dateStrBuf[10]="";
//PID2 pid = {0.5,0.1,0.025,0,0,0,0};
//PID2 pid = {1,0.2,0,0,0,0,0};
//PID2 pid = {1.5,0.4,0.02,0,0,0,0};//最佳参数
PID2 pid = {0.4,0.5,0.02,0,0,0,0};
int duty=0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void Oled_Test(void);
void ADC_Test(void);
void Key_Test(void);
void Uart_Test(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	//uint8_t A15_state=0;//按键状态
	//uint8_t oled_screen=0;//oled界面
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
	
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();
  MX_I2C1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
	OLED_Init();//注意要放在MX_I2C1_Init();后面
	HAL_ADCEx_Calibration_Start(&hadc1);
	HAL_TIM_Base_Start_IT(&htim2);//开启定时器
	//A15_state=HAL_GPIO_ReadPin(GPIOA, 15);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_15, GPIO_PIN_SET);
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);
	Set_Tim3_Duty(&htim3,0);//初始化为不充电
  /* USER CODE END 2 */
		
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
//		Oled_Test();
//		ADC_Test();
//		Key_Test();
//		Uart_Test();
//		A15_state=HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15);
//		if(A15_state==GPIO_PIN_RESET)
//		{
//			HAL_Delay(10);//按键消抖
//			if(++oled_screen==4) oled_screen=0;//0-三个参数显示，1-电压散点图，2-电流散点图，3-功率散点图
//		}
//		switch(oled_screen)
//		{
//			case 0://三个参数显示
//				
//			break;
//			case 1://电压散点图
//				
//			break;
//			case 2://电流散点图
//				
//			break;
//			case 3://功率散点图
//				
//			break;
//		}
      
			//显示电压
			sprintf((char*)dateStrBuf,"V:%4.2f",voltage_ISN);
	    OLED_ShowString(0,0, dateStrBuf,16);
			//显示电流
			sprintf((char*)dateStrBuf,"C:%4.2f",electric_current);
			//sprintf((char*)dateStrBuf,"D:%3d",(int)pid.pidout);
	    OLED_ShowString(0,2, dateStrBuf,16);
			//显示功率
			sprintf((char*)dateStrBuf,"P:%4.2f",power);
	    OLED_ShowString(0,4, dateStrBuf,16);
			//显示占空比
			sprintf((char*)dateStrBuf,"duty:%2d %f",duty,pid.pidout);
	    OLED_ShowString(0,6, dateStrBuf,16);
			HAL_Delay(50);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void Oled_Test(void)
{
	float date=14.099;
	unsigned char dateStrBuf[10]="";
	OLED_Clear();
	OLED_ShowCHinese(0,0,0);
	OLED_ShowCHinese(18,0,1);
	OLED_ShowCHinese(36,0,2);
	OLED_ShowCHinese(54,0,3);	
	OLED_ShowCHinese(72,0,4);
	OLED_ShowCHinese(90,0,5);
	OLED_ShowCHinese(108,0,6);
	OLED_ShowCHinese(0,2,7);
	OLED_ShowCHinese(18,2,8);
	OLED_ShowCHinese(36,2,9);
	HAL_Delay(700);
	OLED_Clear();


	sprintf((char*)dateStrBuf,"%04.2f",date);
	OLED_ShowString(0,0, dateStrBuf,16);	 
	//OLED_ShowNum(0,0,777,3,16);
	HAL_Delay(3000);
	OLED_Clear();
	
	OLED_DrawBMP(30,0,94,8,BMP1);  //图片显示(图片显示慎用，生成的字表较大，会占用较多空间，FLASH空间8K以下慎用)
	HAL_Delay(3000);
	OLED_Clear();
}
void ADC_Test(void)
{
	HAL_ADC_Start_DMA(&hadc1,ADC_Value,4);
	sprintf((char*)dateStrBuf,"%4.6f",ADC_Value[0]*3.3/4096.0);
	OLED_ShowString(0,0, dateStrBuf,16);
	sprintf((char*)dateStrBuf,"%4.6f",ADC_Value[1]*3.3/4096.0);
	OLED_ShowString(0,2, dateStrBuf,16);
	sprintf((char*)dateStrBuf,"%4.2f",ADC_Value[2]*3.3/4096.0);
	OLED_ShowString(0,4, dateStrBuf,16);
	sprintf((char*)dateStrBuf,"%4.2f",ADC_Value[3]*3.3/4096.0);
	OLED_ShowString(0,6, dateStrBuf,16);

	HAL_Delay(1000);
	OLED_Clear();
}
void Key_Test(void)
{
	GPIO_PinState key_state;
	unsigned char dateStrBuf[10]="";
	unsigned char duty=50;
	
	key_state=HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15);
	if(key_state==GPIO_PIN_SET)
	{
		sprintf((char*)dateStrBuf,"%s","key pressed");
		OLED_ShowString(0,0, dateStrBuf,16);
		Set_Tim1_Duty(&htim1,50);
	  Set_Tim3_Duty(&htim3,50);
	}
	else
	{
		sprintf((char*)dateStrBuf,"%s","key unpressed");
		OLED_ShowString(0,0, dateStrBuf,16);
		Set_Tim1_Duty(&htim1,duty++);
	  Set_Tim3_Duty(&htim3,duty++);
	}
	HAL_Delay(200);
	OLED_Clear();	
}
//定时器2的50ms定时中断函数
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim == (&htim2))
    {
			for(int i=0;i<40;i++)
			{
				HAL_ADC_Start_DMA(&hadc1,ADC_Value,4);
				voltage_ISN_sum+=ADC_Value[1]*3.3/4096.0*11;
				electric_current_sum+=(ADC_Value[3]*33/4096.0-16.8);
			}
			voltage_ISN=voltage_ISN_sum/40;
			electric_current=electric_current_sum/40;
			voltage_ISN_sum=0;
			electric_current_sum=0;
			if(electric_current<0)
			{
				electric_current=0;
			}
			power=voltage_ISN*electric_current;
			pid.err=power_want-power;
			//控制尖峰的变化
//			if(pid.err<-2.2)  //超出10W，增大i消去尖峰
//			{
//				pid.iGain=25;
//			}
//			else
//			{
//				pid.iGain=0.5;
//			}
			Add_pid_update(&pid);
			duty+=(int)pid.pidout;
			if(duty>100)//限制功率
			{
				duty=100;
			}
			if(duty<0)
			{
				duty=0;
			}
			//duty=100;
			Set_Tim3_Duty(&htim3,duty);
			printf("%04.2f %04.2f %04.2f %03d %04.2f\r\n",voltage_ISN,electric_current,power,duty,pid.err);
    }
}
int fputc(int ch, FILE *f)
{
 uint8_t temp[1] = {ch};
 HAL_UART_Transmit(&huart1, temp, 1, 2);
 return ch;
}
void Uart_Test(void)
{
	voltage_ISN+=0.1;
	electric_current+=0.1;
	printf("%04.2f %04.2f %04.2f\r\n",voltage_ISN,electric_current,power);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
